# pay-4-words
Creates an invoice based on word count from input files.

## Getting started
* Put the invoice template in the root directory and name it `invoice_template.docx`
* Make sure to have a following strings as placeholders in the template:
  - In table:
    + `<file_name>`
    + `<price_for_page>`
    + `<total_pages>`
    + `<doc_price>`
    + `<no_tax_placeholder>`
    + `<tax_placeholder>`
    + `<total_placeholder>`
  - Anywhere in the document
    + `<total_in_words>`
    + `<invoice_id>`
    + `<date_str>`
* Put the source files inside `source_files` directory. Only .docx files are supported. If there are any .doc files, use convert_doc2docx.vbs script to convert them to .docx using Microsoft Word (if it is installed)
* Run the script on Windows by running a `generate_invoice.bat` script or directly from console:
> python3.10 main.py
* One invoice containing all the documents will be generated inside `output_files` directory

### Pre-requisites

Create a python virtual environment and install the requirements with following commands in project directory.
> python3.10 -m venv venv
> source venv/bin/activate
> pip install -r requirements.txt

On Windows install python3.10 and run `setup_venv` at project's root directory

## pre-commit

To make sure, there are no code or formatting errors, before every commit, run:
> pre-commit run --all-files
