bRecursive = False
sFolder = "source_files"
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oWord = CreateObject("Word.Application")
oWord.Visible = True

Set oFolder = oFSO.GetFolder(sFolder)
ConvertFolder(oFolder)
oWord.Quit

Sub ConvertFolder(oFldr)
    For Each oFile In oFldr.Files
        If LCase(oFSO.GetExtensionName(oFile.Name)) = "doc" Then
            Set oDoc = oWord.Documents.Open(oFile.path)
            oWord.ActiveDocument.SaveAs oFile.path & "x", 12
            oDoc.Close
        End If
    Next

    If bRecursive Then
        For Each oSubfolder In oFldr.Subfolders
            ConvertFolder oSubfolder
        Next
    End If
End Sub
