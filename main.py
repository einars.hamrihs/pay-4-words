from src.document_processor import Invoice
import time

if __name__ == "__main__":
    i = Invoice()
    output = i.generate_invoice()
    print(f"Invoice successfully generated at {output}")
    time.sleep(3)
