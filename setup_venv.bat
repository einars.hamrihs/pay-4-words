echo off
echo Creating a python virtual environment
python -m venv venv
echo Installing python requirements in python virtual environment
venv\Scripts\activate & pip install -r requirements.txt & deactivate venv
