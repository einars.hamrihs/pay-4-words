from datetime import datetime

month_locative_cases = {
    1: "janvārī",
    2: "februārī",
    3: "martā",
    4: "aprīlī",
    5: "maijā",
    6: "jūnijā",
    7: "jūlijā",
    8: "augustā",
    9: "septembrī",
    10: "oktobrī",
    11: "novembrī",
    12: "decembrī",
}


class DatetimeLocalizer:
    def __init__(self, datetime_val: datetime):
        self.datetime_val = datetime_val

    def get_locative(self):
        month = month_locative_cases[self.datetime_val.month]
        return self.datetime_val.strftime(f"%Y. gada %d.{month}")
