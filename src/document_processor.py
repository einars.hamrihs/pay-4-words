import os
from docx import Document
from os.path import isfile, join, basename
from typing import NoReturn, List
from copy import deepcopy
from src.datetime_localizer import DatetimeLocalizer
from src.number_pronouncer import NominativeCase
import time
from datetime import datetime


class InvalidInvoiceTemplate(Exception):
    pass


class CannotFindSourceFiles(Exception):
    pass


class SourceDocument:
    """
    Reads and returns data from source files
    """

    def __init__(self, path: str):
        self.path = path

    def read_text(self) -> str:
        document = Document(self.path)
        doc_text = "\n\n".join(paragraph.text for paragraph in document.paragraphs)
        return doc_text

    @staticmethod
    def count_words(text: str) -> int:
        return len(text.split())

    @staticmethod
    def count_characters(text: str) -> int:
        return len(text)

    def process_word_count(self) -> int:
        text = self.read_text()
        word_count = self.count_words(text)
        return word_count

    def calculate_page_count(self) -> float:
        text = self.read_text()
        character_count = self.count_characters(text)
        page_count = round(character_count / 1800, 2)
        return page_count


class Invoice:
    """
    Creates and formats the invoice file
    """

    def __init__(
        self,
        template_path: str = "invoice_template.docx",
        temp_template_path: str = "invoice_template-temp.docx",
        output_path: str = "output_files",
    ):
        self.template = Document(template_path)
        self.temp_template_path = temp_template_path
        self.output_path = output_path
        self.invoice_id = int(time.time())
        self.rate = 6.50
        self.invoice_date = DatetimeLocalizer(datetime.now()).get_locative()
        self.tax = 0.21

    def create_temp_template(self, file_count: int):
        """
        Create a temporary template file from original template by adding required rows depending on how many documents are added.
        """
        for table in self.template.tables:
            for row in table.rows:
                cell_paragraphs = []
                for cell in row.cells:
                    for paragraph in cell.paragraphs:
                        cell_paragraphs.append(paragraph.text)
                if set(
                    ["<file_name>", "<price_for_page>", "<total_pages>", "<doc_price>"]
                ).issubset(cell_paragraphs):
                    for count in range(file_count - 1):
                        temp_row = row
                        border_copied = deepcopy(temp_row._tr)
                        row._tr.addnext(border_copied)
                    self.template.save(self.temp_template_path)
                    return self.temp_template_path

        raise InvalidInvoiceTemplate(
            "Invoice template does not contain one of the following placeholders: <file_name>",
            "<price_for_page>",
            "<total_pages>",
            "<cost>",
        )

    @staticmethod
    def format_float(input_float: float):
        """
        Adds trailing zeros to float
        """
        return "{:.2f}".format(input_float)

    def write_invoice(self, source_list: list, total_price: float) -> NoReturn:
        """
        Writes data to invoice file and calculates totals
        """
        temp_template = Document(self.temp_template_path)

        for paragraph in temp_template.paragraphs:
            if "<date_str>" in paragraph.text:
                paragraph.text = paragraph.text.replace("<date_str>", self.invoice_date)
            elif "<total_in_words>" in paragraph.text:
                total = total_price + (total_price * self.tax)
                intpart = NominativeCase(int(total))
                decimalpart = NominativeCase(round((total - int(total)) * 100))
                total_in_words = f"{intpart.get_nominative()} eiro"
                if decimalpart != 0:
                    total_in_words += f" un {decimalpart.get_nominative()} centi"
                paragraph.text = paragraph.text.replace(
                    "<total_in_words>", total_in_words
                )

        for table in temp_template.tables:
            source_num = 0
            for row in table.rows:
                filled_row = False
                for cell in row.cells:
                    for paragraph in cell.paragraphs:
                        if paragraph.text == "<file_name>":
                            paragraph.text = source_list[source_num]["filename"]
                            filled_row = True
                        elif paragraph.text == "<price_for_page>":
                            paragraph.text = self.format_float(
                                source_list[source_num]["price_per_page"]
                            )
                            filled_row = True
                        elif paragraph.text == "<total_pages>":
                            paragraph.text = self.format_float(
                                source_list[source_num]["page_count"]
                            )
                            filled_row = True
                        elif paragraph.text == "<doc_price>":
                            paragraph.text = self.format_float(
                                source_list[source_num]["doc_price"]
                            )
                            filled_row = True
                        elif paragraph.text == "<no_tax_placeholder>":
                            paragraph.text = self.format_float(total_price)
                        elif paragraph.text == "<tax_placeholder>":
                            paragraph.text = self.format_float(total_price * self.tax)
                        elif paragraph.text == "<total_placeholder>":
                            paragraph.text = self.format_float(
                                total_price + (total_price * self.tax)
                            )

                if filled_row:
                    source_num += 1

        output_filename = (
            self.output_path + "/invoice_" + str(self.invoice_id) + ".docx"
        )
        temp_template.save(output_filename)
        return output_filename

    def generate_invoice(self) -> NoReturn:
        """
        Wrapper for all functions to read source files and generate invoice file
        """
        files = SourceFiles()
        source_files = files.find_files()
        file_count = len(source_files)
        if not file_count:
            raise CannotFindSourceFiles(
                "No .docx files found on source_files directory"
            )
        source_list = []
        total_price = 0.0
        for file in source_files:
            d = SourceDocument(file)
            page_count = d.calculate_page_count()
            doc_price = round(self.rate * page_count, 2)
            source_list.append(
                {
                    "filename": basename(file),
                    "page_count": page_count,
                    "price_per_page": round(self.rate, 2),
                    "doc_price": doc_price,
                }
            )
            total_price += doc_price
        self.create_temp_template(file_count)
        output_filename = self.write_invoice(source_list, total_price)
        os.remove(self.temp_template_path)
        return output_filename


class SourceFiles:
    """
    Reads source files directory and returns doc and docx files
    """

    def __init__(self, source_path: str = "source_files"):
        self.path = source_path

    def find_files(self) -> List[str]:
        doc_files = []
        for f in os.listdir(self.path):
            file_name = join(self.path, f)
            if isfile(file_name) and (file_name.endswith(".docx")):
                doc_files.append(file_name)
        return doc_files
