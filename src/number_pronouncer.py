nominative_pronunciations = {
    "0": "nulle",
    "1": "viens",
    "2": "divi",
    "3": "trīs",
    "4": "četri",
    "5": "pieci",
    "6": "seši",
    "7": "septiņi",
    "8": "astoņi",
    "9": "deviņi",
    "10": "desmit",
    "11": "vienpadsmit",
    "12": "divpadsmit",
    "13": "trīspadsmit",
    "14": "četrpadsmit",
    "15": "piecpadsmit",
    "16": "sešpadsmit",
    "17": "septiņpadsmit",
    "18": "astoņpadsmit",
    "19": "deviņpadsmit",
    "20": "divdesmit",
    "30": "trīsdesmit",
    "40": "četrdesmit",
    "50": "piecdesmit",
    "60": "sešdesmit",
    "70": "septiņdesmit",
    "80": "astoņdesmit",
    "90": "deviņdesmit",
    "100": "simts",
    "200": "divsimt",
    "300": "trīssimt",
    "400": "četrsimt",
    "500": "piecsimt",
    "600": "sešsimt",
    "700": "septiņsimt",
    "800": "astoņsimt",
    "900": "deviņsimt",
    "1xx": "simti",
    "1xu": "simtu",
    "1000": "viens tūkstotis",
    "xxxx": "tūkstoši",
}


class ValidationError(Exception):
    pass


def validate_number(number):
    try:
        if number > 99999:
            raise ValidationError("Cannot pronounce numbers bigger than 99999")
        else:
            return number
    except TypeError:
        raise ValidationError("Only numbers from 0 to 99999 are accepted")


class NominativeCase:
    def __init__(self, number: int):
        self.number = validate_number(number)
        self.number_str = str(number)

    @staticmethod
    def get_tens(number_str: str):

        if number_str[0:1] == "1":
            return nominative_pronunciations[number_str]
        else:
            if number_str[1:2] == "0":
                return nominative_pronunciations[number_str]
            else:
                return (
                    nominative_pronunciations[number_str[0:1] + "0"]
                    + " "
                    + nominative_pronunciations[number_str[1:2]]
                )

    def get_hundreds(self, number_str: str):
        if number_str[0:1] == "1":
            hundred = nominative_pronunciations["1xu"]
        else:
            hundred = nominative_pronunciations[number_str[0:1] + "00"]

        if number_str[1:3] == "00":
            return nominative_pronunciations[number_str]
        elif number_str[1:2] == "0" and number_str[2:3] != "0":
            return hundred + " " + nominative_pronunciations[number_str[2:3]]
        else:
            return hundred + " " + self.get_tens(number_str[1:3])

    def get_thousands(self, number_str: str):
        if number_str[0:1] == "1":
            thousand = nominative_pronunciations["1000"]
        else:
            thousand = (
                nominative_pronunciations[number_str[0:1]]
                + " "
                + nominative_pronunciations["xxxx"]
            )

        if number_str[1:4] == "000":
            return thousand
        elif number_str[1:3] == "00" and number_str[3:4] != "0":
            return thousand + " " + nominative_pronunciations[number_str[3:4]]
        elif number_str[1:2] == "0" and number_str[2:4] != "00":
            return thousand + " " + self.get_tens(number_str[2:4])
        else:
            return thousand + " " + self.get_hundreds(number_str[1:4])

    def get_tens_of_thousands(self, number_str: str):
        if number_str[1:2] == "1":
            thousand = (
                nominative_pronunciations[number_str[0:1] + "0"]
                + " "
                + nominative_pronunciations["1000"]
            )
        else:
            thousand = (
                self.get_tens(number_str[0:2]) + " " + nominative_pronunciations["xxxx"]
            )

        if number_str[2:5] == "000":
            return thousand
        elif number_str[2:4] == "00" and number_str[4:5] != "0":
            return thousand + " " + nominative_pronunciations[number_str[4:5]]
        elif number_str[2:3] == "0" and number_str[3:5] != "00":
            return thousand + " " + self.get_tens(number_str[3:5])
        else:
            return thousand + " " + self.get_hundreds(number_str[2:5])

    def get_nominative(self):
        if len(self.number_str) == 1:
            return nominative_pronunciations[self.number_str]
        elif len(self.number_str) == 2:
            return self.get_tens(self.number_str)
        elif len(self.number_str) == 3:
            return self.get_hundreds(self.number_str)
        elif len(self.number_str) == 4:
            return self.get_thousands(self.number_str)
        elif len(self.number_str) == 5:
            return self.get_tens_of_thousands(self.number_str)
