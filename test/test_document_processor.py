import pytest

from src.document_processor import (
    SourceDocument,
    Invoice,
    SourceFiles,
    InvalidInvoiceTemplate,
    CannotFindSourceFiles,
)
from src.datetime_localizer import DatetimeLocalizer
from test.samples.sample_docx_content import sample_docx_string
from docx import Document
import os
from datetime import datetime
from unittest import mock
import shutil
from pytest import raises


test_source_list = [
    {
        "filename": "sample-docx-file-for-testing.docx",
        "page_count": 15.36,
        "price_per_page": 6.50,
        "doc_price": 99.84,
    },
    {
        "filename": "sample-docx-file-for-testing2.docx",
        "page_count": 3.29,
        "price_per_page": 6.50,
        "doc_price": 21.39,
    },
]

test_source_files = [
    "test/samples/source_files/sample-docx-file-for-testing.docx",
    "test/samples/source_files/sample-docx-file-for-testing2.docx",
]


def test_read_text():
    d = SourceDocument("test/samples/source_files/sample-docx-file-for-testing.docx")
    assert sample_docx_string == d.read_text()


def test_count_words():
    d = SourceDocument("test/samples/source_files/sample-docx-file-for-testing.docx")
    assert 4082 == d.count_words(sample_docx_string)


def test_count_characters():
    d = SourceDocument("test/samples/source_files/sample-docx-file-for-testing.docx")
    assert 27639 == d.count_characters(sample_docx_string)


def test_process_word_count():
    d = SourceDocument("test/samples/source_files/sample-docx-file-for-testing.docx")
    assert 4082 == d.process_word_count()


def test_calculate_page_count():
    d = SourceDocument("test/samples/source_files/sample-docx-file-for-testing.docx")
    assert 15.36 == d.calculate_page_count()


def test_create_temp_template():
    i = Invoice("test/samples/test-invoice_template.docx")
    test_output = Document(i.create_temp_template(2))
    shutil.copyfile(
        "test/samples/test-invoice_template-temp-sample.docx",
        "test/samples/test-invoice_template-temp.docx",
    )
    sample_output = Document("test/samples/test-invoice_template-temp.docx")
    assert "\n\n".join(
        paragraph.text for paragraph in sample_output.paragraphs
    ) == "\n\n".join(paragraph.text for paragraph in test_output.paragraphs)
    os.remove("test/samples/test-invoice_template-temp.docx")
    os.remove("invoice_template-temp.docx")


def test_create_temp_template_invalid():
    with raises(InvalidInvoiceTemplate):
        i = Invoice("test/samples/test-invoice_template-invalid.docx")
        Document(i.create_temp_template(2))


def test_format_float():
    i = Invoice("test/samples/test-invoice_template.docx")
    assert "99.00" == i.format_float(99.00)


def test_write_invoice():
    with mock.patch("time.time", return_value=1652777432.0):
        with mock.patch(
            "src.document_processor.datetime",
            mock.Mock(now=mock.Mock(return_value=datetime(2022, 5, 17, 15, 14, 18))),
        ):
            shutil.copyfile(
                "test/samples/test-invoice_template-temp-sample.docx",
                "test/samples/test-invoice_template-temp.docx",
            )
            i = Invoice(
                "test/samples/test-invoice_template.docx",
                temp_template_path="test/samples/test-invoice_template-temp.docx",
                output_path="test/samples/output_files",
            )
            test_invoice_path = i.write_invoice(
                test_source_list,
                121.23,
            )
            test_output = Document(test_invoice_path)
            sample_output = Document("test/samples/invoice_sample.docx")
            assert "\n\n".join(
                paragraph.text for paragraph in sample_output.paragraphs
            ) == "\n\n".join(paragraph.text for paragraph in test_output.paragraphs)

            sample_table_output_list = []
            for table in sample_output.tables:
                for row in table.rows:
                    for cell in row.cells:
                        for paragraph in cell.paragraphs:
                            sample_table_output_list.append(paragraph.text)

            test_table_output_list = []
            for table in test_output.tables:
                for row in table.rows:
                    for cell in row.cells:
                        for paragraph in cell.paragraphs:
                            test_table_output_list.append(paragraph.text)

            assert "\n\n".join(sample_table_output_list) == "\n\n".join(
                test_table_output_list
            )

    os.remove("test/samples/test-invoice_template-temp.docx")
    os.remove("test/samples/output_files/invoice_1652777432.docx")


def test_generate_invoice():
    with mock.patch("time.time", return_value=1652777432.0):
        with mock.patch(
            "src.document_processor.SourceFiles.find_files",
            return_value=test_source_files,
        ):
            with mock.patch(
                "src.document_processor.datetime",
                mock.Mock(
                    now=mock.Mock(return_value=datetime(2022, 5, 17, 15, 14, 18))
                ),
            ):
                shutil.copyfile(
                    "test/samples/test-invoice_template-temp-sample.docx",
                    "test/samples/test-invoice_template-temp.docx",
                )
                i = Invoice(
                    "test/samples/test-invoice_template.docx",
                    temp_template_path="test/samples/test-invoice_template-temp.docx",
                    output_path="test/samples/output_files",
                )
                test_invoice_path = i.generate_invoice()
                test_output = Document(test_invoice_path)
                sample_output = Document("test/samples/invoice_sample.docx")
                assert "\n\n".join(
                    paragraph.text for paragraph in sample_output.paragraphs
                ) == "\n\n".join(paragraph.text for paragraph in test_output.paragraphs)

                sample_table_output_list = []
                for table in sample_output.tables:
                    for row in table.rows:
                        for cell in row.cells:
                            for paragraph in cell.paragraphs:
                                sample_table_output_list.append(paragraph.text)

                test_table_output_list = []
                for table in test_output.tables:
                    for row in table.rows:
                        for cell in row.cells:
                            for paragraph in cell.paragraphs:
                                test_table_output_list.append(paragraph.text)

                assert "\n\n".join(sample_table_output_list) == "\n\n".join(
                    test_table_output_list
                )

    os.remove("test/samples/output_files/invoice_1652777432.docx")


def test_generate_invoice_with_empty_input():
    with raises(CannotFindSourceFiles):
        with mock.patch("time.time", return_value=1652777432.0):
            with mock.patch(
                "src.document_processor.SourceFiles.find_files", return_value=[]
            ):
                shutil.copyfile(
                    "test/samples/test-invoice_template-temp-sample.docx",
                    "test/samples/test-invoice_template-temp.docx",
                )
                i = Invoice(
                    "test/samples/test-invoice_template.docx",
                    temp_template_path="test/samples/test-invoice_template-temp.docx",
                    output_path="test/samples/output_files",
                )
                i.generate_invoice()
    os.remove("test/samples/test-invoice_template-temp.docx")


def test_find_files():
    dp = SourceFiles("test/samples/source_files")
    assert [
        "test/samples/source_files/sample-docx-file-for-testing2.docx",
        "test/samples/source_files/sample-docx-file-for-testing.docx",
    ] == dp.find_files()


def test_get_locative():
    test_date = datetime(2022, 5, 17, 11, 50, 32)
    dl = DatetimeLocalizer(test_date)
    assert dl.get_locative() == "2022. gada 17.maijā"


def test_temp_files_exist():
    if os.path.exists("test/samples/test-invoice_template-temp.docx") or os.path.exists(
        "invoice_template-temp.docx"
    ):
        pytest.fail(reason="One of the temporary files is not deleted")
