import pytest
from src.number_pronouncer import NominativeCase, ValidationError

nominative_test_values = [
    (0, "nulle"),
    (1, "viens"),
    (3, "trīs"),
    (9, "deviņi"),
    (10, "desmit"),
    (15, "piecpadsmit"),
    (20, "divdesmit"),
    (31, "trīsdesmit viens"),
    (49, "četrdesmit deviņi"),
    (100, "simts"),
    (101, "simtu viens"),
    (104, "simtu četri"),
    (111, "simtu vienpadsmit"),
    (112, "simtu divpadsmit"),
    (122, "simtu divdesmit divi"),
    (151, "simtu piecdesmit viens"),
    (200, "divsimt"),
    (301, "trīssimt viens"),
    (305, "trīssimt pieci"),
    (313, "trīssimt trīspadsmit"),
    (361, "trīssimt sešdesmit viens"),
    (999, "deviņsimt deviņdesmit deviņi"),
    (1000, "viens tūkstotis"),
    (1001, "viens tūkstotis viens"),
    (1016, "viens tūkstotis sešpadsmit"),
    (1092, "viens tūkstotis deviņdesmit divi"),
    (1500, "viens tūkstotis piecsimt"),
    (1110, "viens tūkstotis simtu desmit"),
    (7524, "septiņi tūkstoši piecsimt divdesmit četri"),
    (8511, "astoņi tūkstoši piecsimt vienpadsmit"),
    (10000, "desmit tūkstoši"),
    (12000, "divpadsmit tūkstoši"),
    (14081, "četrpadsmit tūkstoši astoņdesmit viens"),
    (31562, "trīsdesmit viens tūkstotis piecsimt sešdesmit divi"),
    (52001, "piecdesmit divi tūkstoši viens"),
]

invalid_nominative_values = ["100", 100001]


@pytest.mark.parametrize("test_input,expected", nominative_test_values)
def test_get_nominative(test_input, expected):
    n = NominativeCase(test_input)
    assert n.get_nominative() == expected


@pytest.mark.parametrize("test_input", invalid_nominative_values)
def test_get_nominative_invalid_values(test_input):
    with pytest.raises(ValidationError):
        NominativeCase(test_input)
